#!/usr/bin/python3

# rss-format - format batch query results produced by wnpp_by_tags.py
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import sys
import os
import time
import datetime
from glob import glob
from email.utils import formatdate

def syntax():
    print("syntax: %s <src-dir> <dest-dir> [base-url]" % os.path.basename(sys.argv[0]))
    print("src-dir is the directory hierarchy created by running wnpp_by_tags.py")
    print("in batch mode. base-url will be used as prefix for the RSS feed's")
    print("channel url")
    exit(0)

rss_header =\
"""<?xml version="1.0"?>
<rss version="2.0">
<channel>

"""

rss_footer =\
"""
</channel>
</rss>

"""
def main():
    # generate rss entries for bugs that are at most this many days old
    PUBLISH_THRESHOLD = 36500
    if  len(sys.argv) < 2 or '-h' in sys.argv or '--help' in sys.argv:
        syntax()
    src_dir = sys.argv[1]
    dst_dir = sys.argv[2]
    if len(sys.argv) == 3:
        url_prefix = '/'
    else:
        url_prefix = sys.argv[3]
    if not os.path.isdir(src_dir):
        giveup("\"%s\" does not exist or is not a directory" % src_dir)
    for src_facet_dir in glob("%s/*" % src_dir):
        if not os.path.isdir(src_facet_dir):
            continue
        for src_tag_file in glob("%s/*" % src_facet_dir):
            items = []
            for i, line in enumerate(open(src_tag_file).readlines()):
                # the columns are: bug type, bug number, package, popcon, dust
                cols = line.rstrip().split(" ")
                bug_type, bug_no, package_name, popcon, dust = cols[:5]
                try:
                    dust = int(dust)
                except ValueError as e:
                    # presumably too old, don't include in feed
                    print("skipping line {}:{}: {}".format(src_tag_file, i, str(e)))
                    sys.stderr.write("skipping line with invalid dust value \"%s\"" % line)
                    continue
                if len(cols) != 5:
                    sys.stderr.write("skipping invalid line \"%s\"" % line)
                    continue
                if dust <= PUBLISH_THRESHOLD:
                    bts_href = "https://bugs.debian.org/%s" % bug_no
                    pts_href = 'https://tracker.debian.org/pkg/%s' % package_name
                    popcon_href =  'https://qa.debian.org/popcon.php?package=%s' % package_name
                    description =  "\n".join([
                        "Package %s, with popcon %s, is in need of work.\n" % (package_name, popcon),
                        bts_href, pts_href, popcon_href])
                    bug_date = datetime.datetime.today() - datetime.timedelta(days=dust)
                    items.append('\n'.join([
                        "\n\t<item>",
                        "\t\t<title>%s: %s</title>" % (bug_type, package_name),
                        "\t\t<link>https://bugs.debian.org/%s</link>" % bug_no,
                        "\t\t<description>%s</description>" % description,
                        "\t\t<guid>%s</guid>" % bts_href,
                        "\t\t<pubDate>%s</pubDate>" % formatdate(float(bug_date.strftime('%s'))),
                        "\t</item>"]))
            # create rss file
            facet = src_facet_dir.split('/')[-1]
            tag = os.path.basename(src_tag_file)
            os.makedirs('%s/tags/%s' % (dst_dir, facet), exist_ok=True)
            rss_filename = '%s/tags/%s/%s.xml' % (dst_dir, facet, tag)
            print('writing to %s' % rss_filename)
            facet_tag = '%s::%s' % (facet, tag)
            channel_href = "\nhttps://%s/tags/%s/%s.html" % (url_prefix, facet, tag)
            with open(rss_filename, 'w') as rssfile:
                rssfile.writelines([
                    rss_header,
                    "\n<title>Work-needing packages with tag %s</title>" % facet_tag,
                     "\n<description>Get notified for %s packages in need of attention</description>" % facet_tag,
                     "\n<link>%s</link>" % channel_href,
                     #"\n<link rel='self' type='application/xml+rss'>%s</link>" % channel_href,
                    '\n'.join(items),
                    rss_footer])


if __name__ == '__main__':
    main()
