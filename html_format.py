#!/usr/bin/python3

# html-format - format batch query results produced by wnpp_by_tags.py
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

from glob import glob
import os
import sys
import time

from lib.util import create_file, ensure_dir_exists, giveup

html_header =\
"""
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "https://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="https://www.w3.org/1999/xhtml" xml:lang="en">
    <head>
        <title>Browse WNPP bugs based on debtags</title>
        <link href="https://debian.org/debhome.css" rel="stylesheet" type="text/css">
        <link href="https://debian.org/debian.css" rel="stylesheet" type="text/css" media="all">
    </head>

<body style="width: 50%%; margin: 0 auto; font-size: large;">
<h3>Find orphaned, RFA'd, RFH'd packages using debtags</h3>
<div id="navbar">
<p class="hidecss"><a href="#content">Skip Quicknav</a></p>
<ul>
   <li><a href="/">Browse bugs</a></li>
   <li><a href="/about.html">What is this?</a></li>
</ul>
</div> <!-- end navbar -->
<p id="breadcrumbs"> %s</p>
"""

bugs_html_template =\
"""
%s
        <table border="1">
            <tr>
                <th>bug type</th>
                <th>bug number</th>
                <th>package</th>
                <th>popcon</th>
                <th>age (days)</th>
            </tr>
        %s
        </table>
    </body>
</html>
"""

facet_values_html_template =\
"""
%s
        <table border="1">
            <tr>
                <th>%s</th>
                <th>wnpp bugs</th>
                <th>feed</th>
            </tr>
            %s
        </table>
    </body>
</html>
"""

main_page_breacrumb ="<a href='%s/index.html'>facets</a> / "

def syntax():
    print("syntax: %s <src-dir> <dest-dir>" % os.path.basename(sys.argv[0]))
    print("src-dir is the directory hierarchy created by running wnpp_by_tags.py")
    print("in batch mode.")
    exit(0)

def main():
    if  len(sys.argv) < 2 or '-h' in sys.argv or '--help' in sys.argv:
        syntax()
    src_dir = sys.argv[1]
    dst_dir = sys.argv[2]
    if not os.path.isdir(src_dir):
        giveup("\"%s\" does not exist or is not a directory" % src_dir)
    facets = []
    timestamp = "%d-%02d-%02d" % time.gmtime()[:3]
    for src_facet_dir in glob("%s/*" % src_dir):
        if not os.path.isdir(src_facet_dir):
            continue
        facet = src_facet_dir.split("/")[-1]
        facet_values = []
        facets.append(facet)
        for src_tag_file in glob("%s/*" % src_facet_dir):
            facet_value = os.path.basename(src_tag_file)
            dst_facet_dir = "%s/tags/%s" % (dst_dir, facet)
            ensure_dir_exists(dst_facet_dir)
            bug_table_rows = []
            for line in open(src_tag_file).readlines():
                # the columns are: bug type, bug number, package, popcon, dust
                cols = line.rstrip().split(" ")
                _, bug_no, package_name, popcon = cols[:4]
                if len(cols) != 5:
                    sys.stderr.write("skipping invalid line \"%s\"" % line)
                    continue
                # add link for bug number and package
                cols[1] = '<a href="https://bugs.debian.org/%s">%s</a>' \
                        % (bug_no, bug_no)
                cols[2] = '<a href="https://tracker.debian.org/pkg/%s">%s</a>' \
                        % (package_name, package_name)
                cols[3] = '<a href="https://qa.debian.org/popcon.php?package=%s">%s</a>' \
                        % (package_name, popcon)
                cols = "</td><td>".join(cols)
                bug_table_rows.append("<tr><td>%s</td></tr>" % cols)
            nbugs = len(bug_table_rows)
            rss_column = "<td><a class='rss_logo' href=\"./%s.xml\">RSS</a></td>" % facet_value
            if nbugs > 0: row = "<tr><td><a href=\"./%s.html\">%s</a></td><td>%d</td>%s</tr>" \
                        % (facet_value, facet_value, nbugs, rss_column)
            else:
                row = "<tr><td>%s</td><td>0</td>%s</tr>" % (facet_value, rss_column)
            facet_values.append(row)
            dst_tag_file = "%s/%s.html" % (dst_facet_dir, facet_value)
            # FIXME: use newstyle dicts instead
            tag = "%s::%s" % (facet, facet_value)
            breadcrumb = "%s %s %s" % (main_page_breacrumb % "../..",
                    "<a href='../%s/index.html'>%s</a> / %s" % (facet, facet, facet_value),
                    "<a class='rss_logo' href='./%s.xml'>RSS</a>" % facet_value)
            html_doc = bugs_html_template % (html_header % breadcrumb,
                 "\n".join(bug_table_rows))
            create_file(dst_tag_file, str.encode(html_doc))

        breadcrumb = "%s %s" % (main_page_breacrumb % '../..', facet)
        content = facet_values_html_template % \
                (html_header % breadcrumb, facet,
                        "\n".join(sorted(facet_values)))
        create_file("%s/index.html" % dst_facet_dir, str.encode(content))

    formated_facets = ["<li><a href=\"./tags/%s/index.html\">%s</a></li>" % (f, f)
                       for f in facets]
    create_file("%s/index.html" % dst_dir, str.encode(
                ''.join([html_header % (main_page_breacrumb % '.'),
                    "\n".join(sorted(formated_facets)),
                    """<div style="font-size: x-small; text-align: right;">
                    last update: %d-%02d-%02d</div>""" % time.gmtime()[:3],
                    "</body></html>"])))

    create_file("%s/about.html" % dst_dir, str.encode(
            ' '.join([html_header % (main_page_breacrumb % '.'),
                     "<p>This site's purpose is to help potential Debian",
                     "contributors to find packages they'd like to work on.</p>",

                     "<p>The full list of packages in need of work (orphaned",
                     "being-adopted for too long, <acronym title='",
                     "Request For Adoption'>RFA</acronym>'d, or <acronym",
                     "title='Request For Help'>RFH</acronym>'d) is at",
                     "<a href='https://debian.org/devel/wnpp'>debian.org</a>,",
                     "but it's so long that it's tedious to scan it manually.</p>",

                     "<p>Wouldn't it great to be able to eg. see only packages that",
                     "are <a href='./tags/implemented-in/index.html'>implemented</a>",
                     "only in a given language or packages from",
                     "<a href='./tags/suite/index.html'>a given upstream?</a>",
                     "This is exactly what this site aims to do, with a little",
                     "help from",
                     "<a href='https://wiki.debian.org/Debtags'>debtags</a>.",
                     "(See the full ",
                     "<a href='https://debtags.alioth.debian.org/vocabulary/'>",
                     "debtags vocabulary)</a>.</p>",

                     "<p>The code behind this site, available at",
                     "<a href='https://salsa.debian.org/debian/wnpp-by-tags'>",
                     "alioth</a>, is also useful for running more complex",
                     "debtags-based queries.</p>"
                     ])))

if __name__ == '__main__':
    main()
